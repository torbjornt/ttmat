function  [freqs,S,C] = fftex(x,Fs,tt)
% Funksjon for aa gjenskape doemet i Stull, s. 304
% Fouriertransformasjon av vektor x
%   [freqs,S,C] = FFTEX(x,Fs,tt)

% if nargin > 2
%     args = varargin;
% else
%     args = {'not'};
% end
if nargin < 3
    tt = '';
end
if nargin < 2
    Fs = length(x);
end
    
C = fft(x);
N = length(C);
C = C(:)/N;


F = C.*conj(C);


if mod(N,2) == 0
    E = [2 * F(2:N/2) ; F(N/2 + 1)];
else
    E = 2 * F(2:ceil(N/2));
end

fn = length(E);
f_nyq = Fs/2;
freqs = linspace(f_nyq/fn,f_nyq,fn);
if nargin > 1
    S = E/(f_nyq/fn);
else
    S = E;
end
% figure;
% freqs = linspace(0,Fs/2,N/2+1);
% loglog(freqs,pE)
% hold on
% loglog(freqs(150:end-150),pmE(150:end-150),'r','Linewidth',2)
% 
% plot([1e-2,1],[1e-2, 1e-2-5/3*(1-1e-2)],'linewidth',3)
if nargout == 0
    figure
    loglog(freqs,S);
    xlabel('Frequency [Hz]')
    ylabel('Spectral energy density')
    title(tt)
    if N > 10000
        % win = hanning(512);
        m = runavg(S,101);
        hold on
        loglog(freqs,m,'r','LineWidth',2)
    end
end

