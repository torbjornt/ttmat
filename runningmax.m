function [rm,days] = runningmax(x,t)
% Given a time series X and time vector T, return daily maxima and time vector.
% 
%    [rm,days] = runningmax(x,t)

if min(size(x)) > 1
    error('X has to be a vector, sorry. If you want to handle matrices, edit me.')
end

if ischar(t)
    disp('Assuming standard DATESTR format on time string, converting to DATENUM');
    t = datenum(t);
elseif min(size(t)) > 1
    disp('Assuming date matrix, converting to DATENUM')
    t = datenum(t);
end

firstday = ceil(t(1));
lastday  = floor(t(end));
days = firstday:lastday;

rm = NaN(length(days)+1,1);


rmstart = 1;
rmind = 1;
for day = days
    rmstop = find(t>=day,1,'first');
    if isempty(rmstop)
        warning('No end-of-day index found at day %u',day);
        break
    end
    rm(rmind,1) = max(x(rmstart:rmstop-1));
    rmstart=rmstop;
    rmind = rmind+1;
end
if ~isempty(rmstop)
    rm(rmind) = max(x(rmstop:end));
end
days = [days(1)-1, days];