function xd = GetLims(x,p)
% from data x, find maximum and minimum,
% add/subtract 10% of diff, return min-diff,max+diff
if nargin < 2
    p = 0.1;
end
xmin = min(x);
xmax = max(x);

limdiff = (xmax-xmin)*p;

xd = [xmin-limdiff, xmax+limdiff];