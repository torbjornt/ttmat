function [x,y] = progvec_TT(time,vel,dir,varargin)
%
%     progvec_TT(time,velocity,direction)
% where
%   -- time is in datenum-format
%   -- velocity is given in m/s
%   -- direction is given in compass degrees
%
% Given vectors for time, velocity and direction of equal length,
% calculates and plots a progressive vector diagram, marking and labeling
% the start of each day.
%
%     progvec_TT(time,velocity,direction,'label day','no)
% Plots progressive vector without writing the date next to each dot.
%
%     progvec_TT(time,velocity,direction,'mark interval','week')
% Place label every seven days, in stead of every day. Alternatives are
% 'day' (default) and 'month'.
% 
%     progvec_TT(time,velocity,direction,'markers','no')
% Optionally 'none'. Do not label any points on the diagram.
% 
% This function is a modification of Progr_vec.m, the author(s) of
% which I do not know.
% Torbjoern Taskjelle (totaskj@gmail.com)

if length(time) ~= length(dir) && length(time) ~= length(vel)
    error('Time/vel/dir does not all have the same length.')
end

if mod(length(varargin),2) == 1
   error('Sorry, this does not work -- the optional arguments must be PARAMETER,VALUE pairs.');
end

% default values
markSwitch = 'yes';
dayLabel   = 'yes';
markInt    = 'day';

% overwrite with values from varargin
for k = 1:length(varargin)
    if strcmp(varargin{k},'label day')
        dayLabel = varargin{k+1};
    elseif strcmp(varargin{k},'mark interval')
        markInt = varargin{k+1};
    elseif strcmp(varargin{k},'markers')
        markSwitch = varargin{k+1};
    end
end


dim = length(time);
x = zeros(dim+1,1);
y = zeros(dim+1,1);

TimestepInDays = diff(time);
TimestepInDays = [TimestepInDays(:); TimestepInDays(end)];
TimestepInSeconds = TimestepInDays * 86400;
xtmp=0;
ytmp=0;
% calculate x- and y-positions
for i=1:dim,
    x(i+1) = xtmp + vel(i)*sind(dir(i))*TimestepInSeconds(i);
    y(i+1) = ytmp + vel(i)*cosd(dir(i))*TimestepInSeconds(i);
    xtmp=x(i+1);
    ytmp=y(i+1);
end

% convert to km
x = x/1000;
y = y/1000;

% plot progressive vector line
plot(x,y)
hold on
axis equal
if exist('GetLims.m','file')
    xl = GetLims(x,0.05);
    yl = GetLims(y,0.05);
    xlim(xl)
    ylim(yl)
else
    axis tight
end



va(1) = floor(min(x));
va(2) = ceil(max(x));
va(3) = floor(min(y));
va(4) = ceil(max(y));

xfrac = (va(2)-va(1))*.01;

% markers
switch markSwitch
    case {'none','no'}
        % do nothing
    case 'yes'
    switch markInt
        case '6hour'
            MarkStart = ceil(time(1)*6)/6;
            MarkEnd = floor(time(end)*6)/6;
            MarkTimes = MarkStart:1/4:MarkEnd;
            dateSpec = 'HH:MM';
        case 'hour'
            MarkStart = ceil(time(1)*24)/24;
            MarkEnd = floor(time(end)*24)/24;
            MarkTimes = MarkStart:1/24:MarkEnd;
            dateSpec = 'HH:MM';
        case 'day'
            MarkStart = ceil(time(1));
            MarkEnd = floor(time(end));
            MarkTimes = MarkStart:MarkEnd;
            dateSpec = 'dd/mm';
        case 'week'
            MarkStart = ceil(time(1));
            MarkEnd = floor(time(end));
            MarkTimes = MarkStart:7:MarkEnd;
            dateSpec = 'dd/mm';
        case 'month'
            MarkStart = ceil(time(1));
            MarkEnd = floor(time(end));
            TimeMatrix = datevec(MarkStart:MarkEnd);
            NumYears = TimeMatrix(end,1) - TimeMatrix(1,1);
            NumMonths = TimeMatrix(end,2) - TimeMatrix(1,2);
            TotalMonths = NumYears * 12 + NumMonths;
            StartMonth = TimeMatrix(1,2);
            MarkTimes = datenum(TimeMatrix(1,1),StartMonth:StartMonth+TotalMonths,1);
            if NumYears > 0
                dateSpec = 'mmm yy';
            else
                dateSpec = 'mmm';
            end
    end
    
    % plot markers
    for i=MarkTimes
        [~,ind] = min(abs(time-i));
        plot(x(ind),y(ind),'.','MarkerSize',10)
        xtext=x(ind)+xfrac;
        ytext=y(ind);
        if strcmp(dayLabel,'yes')
            text(xtext + 0.01*(va(2)-va(1)),ytext,datestr(time(ind),dateSpec),'FontSize',10);
        end
    end
end


title({...
    'Progressive Vector Diagram',...
    [datestr(time(1)), ' to ', datestr(time(end))]...
    })
xlabel('Distance Eastward [km]')
ylabel('Distance Northward [km]')
