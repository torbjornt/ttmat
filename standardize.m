function y = standardize(x,dim)
% Standardizes columns/rows in matrix x, by subtracting mean and dividing by STD.
%    Y = STANDARDIZE(X)
% Work along dimension DIM, default 2 (standardizes columns)
%    Y = STANDARDIZE(X,DIM)
if nargin < 2
    dim = 2;
end
y = NaN(size(x));
if min(size(x)) == 1
    y = (x-nanmean(x))/nanstd(x);
else
    if dim == 1
        for k = 1:size(x,dim)
            y(k,:) = (x(k,:)-nanmean(x(k,:)))/nanstd(x(k,:));
        end
    elseif dim == 2
        for k = 1:size(x,dim)
            y(:,k) = (x(:,k)-nanmean(x(:,k)))/nanstd(x(:,k));
        end
    end
end