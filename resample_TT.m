function [res,time,count] = resample_TT(x,t,interval,mode)
% 
% NOTE:
% This has NOT been tested thoroughly, will probably break in many cases.
% 
% "Resamples" to given interval by extracting subsequent timeseries of that
% length and calculating mean/max/min for each subset. Hence not a running
% window, as there is no overlap.
% 
%   [res,time,count] = RESAMPLE_TT(x,t,interval,mode)
% 
% INTERVAL is a string, can be 'year', 'month', 'week', 'day', 'hour'.
% MODE is a string, can be 'mean', 'max', 'min'.
% 
% RES is the "resampled" time series
% TIME is a time vector
% COUNT is how many values in the original time series was used in each
% subsample

if nargin < 4
    mode = 'mean';
end
if nargin < 3
    interval = 'day';
end

switch interval
    case 'month'
        if ~isnumeric(t) || size(t,2) ~= 6 || size(t,2) ~= 3
            t = datevec(datenum(t));
        end
        [monthvals, time] = data_by_month(x,t);
        [res,count] = dostuff(monthvals,mode);
        
    case 'day'
        if ~isnumeric(t) || size(t,2) > 1
            t = datenum(t);
        end
        [dayvals,time] = data_by_day(x,t);
        [res,count] = dostuff(dayvals,mode);
        
    case 'hour'
        if ~isnumeric(t) || size(t,2) < 3
            t = datevec(datenum(t));
        end
        [hourvals,time] = data_by_hour(x,t);
        [res,count] = dostuff(hourvals,mode);
        
    case 'year'
        if ~isnumeric(t) || size(t,2) ~= 6 || size(t,2) ~= 3
            t = datevec(datenum(t));
        end
        [yearvals, time] = data_by_year(x,t);
        [res,count] = dostuff(yearvals,mode);
        
    case 'week'
        if ~isnumeric(t) || size(t,2) > 1
            t = datenum(t);
        end
        [weekvals,time] = data_by_week(x,t);
        [res,count] = dostuff(weekvals,mode);        
        
    otherwise
        error('Cannot recognize given interval')
        
end



end



function [res,count] = dostuff(x, mode)
res = NaN(length(x),1);
count = NaN(length(x),1);

switch mode
    case 'mean'
        for k = 1:length(x)
            if ~isempty(x{k})
                res(k) = mean(x{k});
                count(k) = length(x{k});
            else
                res(k) = NaN;
                count(k) = 0;
            end
        end
    case 'max'
        for k = 1:length(x)
            if ~isempty(x{k})
                res(k) = max(x{k});
                count(k) = length(x{k});
            else
                res(k) = NaN;
                count(k) = 0;
            end
        end
    case 'min'
        for k = 1:length(x)
            if ~isempty(x{k})
                res(k) = min(x{k});
                count(k) = length(x{k});
            else
                res(k) = NaN;
                count(k) = 0;
            end
        end
        
    otherwise
        error('Cannot recognize given mode')
end
end

function [monthvals,months] = data_by_month(x,t)

yearstart = t(1,1);
yearstop = t(end,1);
monthstart = t(1,2);
monthstop = t(end,2);
k = 1;
monthvals = {};
for y = yearstart:yearstop
    if y == yearstart
        M = monthstart;
    else
        M = 1;
    end
    if y == yearstop
        MM = monthstop;
    else
        MM = 12;
    end
    for m = M:MM
        ind = find(t(:,1) == y & t(:,2) == m);
        if ~isempty(ind)
            monthvals{k} = x(ind);
        else
            monthvals{k} = [];
        end
        k = k + 1;
    end
end
Nmonths = (yearstop - yearstart + 1) * 12 - (monthstart - 1) - (12 - monthstop);
months = datenum(yearstart,monthstart:monthstart+Nmonths-1,1);

end

function [dayvals, days] = data_by_day(x,t)
days = floor(t(1)):floor(t(end));
dayvals = {};
for k = 1:length(days)
    ind = find(floor(t) == days(k));
    dayvals{k} = x(ind);
end

end

function [yearvals,years] = data_by_year(x,t)
years = t(1,1):t(end,1);
yearvals = {}
for k = 1:length(years)
    ind = find(t(:,1) == years(k));
    yearvals = x(ind);
end
years = datenum(years,1,1);
end

function [hourvals,hours] = data_by_hour(x,t)

yearstart = t(1,1);
yearstop = t(end,1);
monthstart = t(1,2);
monthstop = t(end,2);
daystart = t(1,3);
daystop = t(end,3);
k = 1;
hourvals = {};
for y = yearstart:yearstop
    if y == yearstart
        M = monthstart;
    else
        M = 1;
    end
    if y == yearstop
        MM = monthstop;
    else
        MM = 12;
    end
    for m = M:MM
        if y == yearstart && M == monthstart;
            D = daystart;
        else
            D = 1;
        end
        if datenum(y,M,daystop) ~= datenum(t(end,1:3))
            DD = eomday(y,m);
        else
            DD = daystop;
        end
        for d = D:DD
            if D ~= daystart
                H = 0;
            else
                H = t(1,4);
            end
            if datenum(y,m,D) ~= datenum(t(end,1:3))
                HH = 23;
            else
                HH = t(end,4);
            end
            for h = H:HH
                ind = find(t(:,1) == y & t(:,2) == m & t(:,3) == d & t(:,4) == h);
                if ~isempty(ind)
                    hourvals{k} = x(ind);
                else
                    hourvals{k} = [];
                end
                k = k + 1;
            end
        end
    end
    
end
Nhours = length(hourvals);
hours = datenum([t(1,1:4), 0 0]) + datenum(0,0,0,0:Nhours-1,0,0);
end

function [weekvals,weeks] = data_by_week(x,t)

[dayx,dayt] = data_by_day(x,t);
startweek1 = find(weekday(dayt) == 2,1,'first');
stopweekN = find(weekday(dayt) == 2,1,'last');
weekvals = {};
weekvals{1} = [];
k = 1;
for m = 1:startweek1-1
    weekvals{k} = [weekvals{k}; dayx{m}(:)];
end
Nweeks = (stopweekN - startweek1)/7;
for m = 1:Nweeks
    k = k + 1;
    weekvals{k} = [];
    stw = startweek1 + (m - 1)*7;
    for n = 0:6
        weekvals{k} = [weekvals{k}; dayx{stw + n}(:)];
    end
end
k = k + 1;
weekvals{k} = [];
for m = stopweekN:length(dayt)
    weekvals{k} = [weekvals{k}; dayx{m}(:)];
end

weeks = floor(dayt(startweek1)):7:floor(dayt(stopweekN));
weeks = [weeks(1)-7; weeks(:)];

end

