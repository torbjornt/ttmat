function keepfig(varargin)
% Works the same way as close, except that the specified figure(s)
% are kept, and the remaining figures are closed.
% 
% Torbjørn Taskjelle
% totaskj@gmail.com
% 11/02-2013

if isempty(varargin)
    return
end
if strcmp(varargin{1},'all')
    disp('Seriously?')
    return
end
figs = str2num(cell2mat(reshape(varargin,[],1)));

figHandles = findall(0,'Type','figure');
if isempty(figHandles)
    error('No figures are open')
end

for k = 1:length(figHandles)
    if ~any(ismember(figHandles(k),figs))
        eval(['close ',num2str(figHandles(k))])
    end
end
